import styled from 'styled-components';

const Button = styled.button`
  border: none;
  border-radius: 4px;
  padding: 10px 15px;
  /* background-color: #36b6fb; */
  background-color: ${props => (props.delete ? '#D33F29' : '#36b6fb')};
  color: #fff;
  font-weight: bold;
  font-size: 16px;
  transition: all 0.2s ease;
  width: ${props => (props.size === 'medium' ? '200px' : null)};

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  &:hover {
    background-color: #1fa9f0;
    background-color: ${props => (props.delete ? '#CC0A00' : '#1fa9f0')};
  }

  svg {
    margin-right: 5px;
  }

  a {
    color: #fff;
    text-decoration: none;
  }
`;

export default Button;
