import styled from 'styled-components';

const FormControl = styled.div`
  margin-bottom: 20px;
  width: 100%;
  display: flex;
  flex-direction: column;

  input {
    flex: 1;
    border: 1px solid #bcccd1;
    padding: 10px 15px;
    border-radius: 4px;
    font-size: 16px;
    margin-top: 10px;
  }
`;

export default FormControl;
