import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { DatePickerControl } from './styles';

const DatePickerField = ({ name, value, onChange, title }) => {
  return (
    <DatePickerControl>
      <label>{title}</label>
      <DatePicker
        selected={(value && new Date(value)) || null}
        onChange={val => {
          onChange(name, val);
        }}
        dateFormat="dd/MM/yyyy"
      />
    </DatePickerControl>
  );
};

export default DatePickerField;
