import styled from 'styled-components';

export const DatePickerControl = styled.div`
  display: flex;
  display: flex;
  flex-direction: column;
  margin-bottom: 30px;
  margin-right: 30px;

  input {
    flex: 1;
    border: 1px solid #bcccd1;
    padding: 10px 15px;
    border-radius: 4px;
    font-size: 16px;
    margin-top: 10px;
  }
`;
