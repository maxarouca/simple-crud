import styled from 'styled-components';

const SectionTitle = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 30px;
`;

export default SectionTitle;
