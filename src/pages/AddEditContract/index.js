import React from 'react';
import { Formik, FieldArray, Field } from 'formik';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { FaArrowLeft, FaPlus, FaTrash } from 'react-icons/fa';
import Container from '../../components/Container';
import Button from '../../components/Button';
import api from '../../services/api';
import { Form } from './styles';
import SectionTitle from '../../components/SectionTitle';
import FormControl from '../../components/FormControl';
import DatePicker from '../../components/DatePicker';

class EditContract extends React.Component {
  state = {
    add: false,
    contract: {
      title: '',
      startDate: new Date(),
      endDate: new Date(),
      parts: [
        {
          name: '',
          lastName: '',
          email: '',
          cpf: '',
          phone: ''
        }
      ]
    }
  };

  componentDidMount() {
    this.loadData();
    if (this.props.match.url === '/add') {
      this.setState({
        add: true
      });
    }
  }

  loadData = async () => {
    if (!this.props.match.url === '/add') {
      const { id } = this.props.match.params;
      try {
        const resolve = await api.get(`/contracts/${id}`);
        this.setState({
          contract: resolve.data
        });
      } catch (error) {
        toast.error('Erro ao carregar os dados.');
      }
    }
  };

  handleAddPart = () => {
    const { contract } = this.state;
    const part = {
      name: '',
      lastName: '',
      email: '',
      cpf: '',
      phone: ''
    };

    const { parts } = contract;
    parts.push(part);

    this.setState({
      contract: {
        ...contract,
        parts
      }
    });
  };

  handleRemovePart = index => {
    const { contract } = this.state;
    const { parts } = contract;

    parts.splice(index, index);

    this.setState({
      contract: {
        ...contract,
        parts
      }
    });
  };

  render() {
    const { contract, add } = this.state;
    return (
      <Container>
        <SectionTitle>
          <h1>{add ? 'Adicionar Contrato' : 'Editar Contrato'}</h1>
          <Button>
            <Link to="/">
              <FaArrowLeft />
              Voltar a página inicial
            </Link>
          </Button>
        </SectionTitle>
        <Formik
          initialValues={contract}
          enableReinitialize
          onSubmit={async values => {
            if (add) {
              const resolve = await api.post('/contracts', values);
              if (resolve.status === 201) {
                toast.success('Cadastro efetuado com sucesso');
                return this.props.history.push('/');
              }
            }
            const { id } = this.props.match.params;
            const resolve = await api.put(`/contracts/${id}`, values);
            if (resolve.status === 200) {
              toast.success('Cadastro efetuado com sucesso');
              this.props.history.push('/');
            }
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            setFieldValue
          }) => (
            <Form onSubmit={handleSubmit}>
              <FormControl>
                <label>Título do contrato</label>
                <input
                  name="title"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.title}
                />
              </FormControl>
              <div style={{ display: 'flex', flexDirection: 'row' }}>
                <DatePicker
                  name="startDate"
                  title="Data de Início"
                  value={values.startDate}
                  onChange={setFieldValue}
                />
                <DatePicker
                  name="endDate"
                  title="Vencimento"
                  value={values.endDate}
                  onChange={setFieldValue}
                />
              </div>
              <SectionTitle>
                <h3>Partes envolvidas</h3>
                <Button type="button" onClick={this.handleAddPart}>
                  <FaPlus /> Adicionar parte
                </Button>
              </SectionTitle>
              <FieldArray
                name="values.parts"
                render={arrayHelpers => (
                  <div>
                    {values.parts.map((parts, index) => (
                      <div id="parts" key={index}>
                        <div style={{ marginBottom: 20 }}>
                          <h4>Parte {index + 1}</h4>
                          <Button
                            delete
                            onClick={() => this.handleRemovePart(index)}
                          >
                            <FaTrash />
                            Remover parte
                          </Button>
                        </div>
                        <FormControl>
                          <label>Nome</label>
                          <Field name={`parts.${index}.name`} />
                        </FormControl>
                        <FormControl>
                          <label>Sobrenome</label>
                          <Field name={`parts.${index}.lastName`} />
                        </FormControl>
                        <FormControl>
                          <label>E-mail</label>
                          <Field name={`parts.${index}.email`} />
                        </FormControl>
                        <FormControl>
                          <label>CPF</label>
                          <Field name={`parts.${index}.cpf`} />
                        </FormControl>
                        <FormControl>
                          <label>Telefone</label>
                          <Field name={`parts.${index}.phone`} />
                        </FormControl>
                      </div>
                    ))}
                  </div>
                )}
              />
              <Button size="medium" type="submit">
                {add ? 'Adicionar Contrato' : 'Editar Contrato'}
              </Button>
            </Form>
          )}
        </Formik>
      </Container>
    );
  }
}

export default EditContract;
