import styled from 'styled-components';

export const Form = styled.form`
  margin-top: 30px;
  display: flex;
  flex-direction: column;

  h3 {
    font-size: 16px;
    margin-bottom: 15px;
  }

  h4 {
    font-size: 14px;
    margin-bottom: 15px;
  }

  button {
    align-self: flex-end;
  }

  div#parts {
    border-bottom: 3px solid #bcccd1;
    margin-bottom: 30px;
    padding-bottom: 15px;
  }
`;
