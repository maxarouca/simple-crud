import styled, { keyframes, css } from 'styled-components';

const rotate = keyframes`
  from{
    transform: rotate(0deg)
  }
  to {
    transform: rotate(360deg)
  }
`;

export const SubmitButton = styled.button.attrs(props => ({
  type: 'submit',
  disabled: props.loading
}))`
  background: #7159c1;
  border: 0;
  padding: 0 15px;
  margin-left: 10px;
  border-radius: 4px;

  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  &[disabled] {
    cursor: not-allowed;
    opacity: 0.6;
  }

  ${props =>
    props.loading &&
    css`
      svg {
        animation: ${rotate} 2s linear infinite;
      }
    `}
`;

export const Loading = styled.div`
  width: 100%;
  height: 100%;
  margin: 30px auto;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  svg {
    animation: ${rotate} 2s linear infinite;
  }
`;

export const List = styled.ul`
  padding: 0;
  list-style: none;

  li {
    padding: 15px 0;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    & + li {
      border-top: 1px solid #bcccd1;
    }

    h3 {
      color: #1fa9f0;
      margin-bottom: 15px;
      font-size: 18px;

      & + div {
        display: flex;

        p {
          margin-right: 20px;
        }
      }
    }

    h4 {
      margin: 15px 0px;
      font-size: 16px;

      & + div {
        display: flex;
        flex-direction: row;
        margin-bottom: 10px;

        div {
          margin: 0 10px;
          padding: 15px;
          border: 1px solid #bcccd1;
          border-radius: 4px;

          &:first-child {
            margin-left: 0;
          }

          p {
            margin-bottom: 5px;
          }
        }
      }
    }
  }
`;

export const ActionButtons = styled.div`
  display: flex;
  margin-top: 10px;

  button {
    margin-right: 10px;
  }
`;
