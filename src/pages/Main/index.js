import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  FaGithubAlt,
  FaPlus,
  FaSpinner,
  FaTrash,
  FaEdit
} from 'react-icons/fa';
import { formatToCPF, formatToPhone } from 'brazilian-values';
import { format } from 'date-fns';
import { toast } from 'react-toastify';
import { parseISO } from 'date-fns/esm';
import api from '../../services/api';
import Container from '../../components/Container';
import Button from '../../components/Button';
import SectionTitle from '../../components/SectionTitle';
import { Loading, List, ActionButtons } from './styles';

class Main extends Component {
  state = {
    loading: false,
    data: []
  };

  async componentDidMount() {
    this.setState({ loading: true });
    this.loadData();
  }

  loadData = async () => {
    try {
      const resolve = await api.get('/contracts');
      this.setState({
        data: resolve.data,
        loading: false
      });
    } catch (error) {
      toast.error('Erro ao carregar os dados.');
    }
  };

  handleDelete = async id => {
    try {
      const resolve = await api.delete(`/contracts/${id}`);
      if (resolve.status === 200) {
        toast.success('Contrato excluído com sucesso!');
        this.loadData();
      }
    } catch (error) {
      toast.error('Erro ao excluir o contrato.');
    }
  };

  render() {
    const { data, loading } = this.state;
    return (
      <Container>
        <h1>
          <FaGithubAlt />
          Simple Contact CRUD
        </h1>
        <SectionTitle>
          <h2>Lista de contratos cadastrados</h2>
          <Button type="button">
            <Link to="/add">
              <FaPlus /> Adicionar contrato
            </Link>
          </Button>
        </SectionTitle>

        <List>
          {loading && (
            <Loading>
              <FaSpinner color="#1FA9F0" size={30} />
            </Loading>
          )}
          {data.map(item => (
            <li key={item.id}>
              <h3>{item.title}</h3>
              <div>
                <p>
                  <strong>Data de Início: </strong>
                  {format(parseISO(item.startDate), 'dd/MM/yyyy')}
                </p>
                <p>
                  <strong>Vencimento: </strong>
                  {format(parseISO(item.endDate), 'dd/MM/yyyy')}
                </p>
              </div>
              <h4>Partes envolvidas</h4>
              <div>
                {item.parts.length > 0 &&
                  item.parts.map(part => (
                    <div key={part.cpf}>
                      <p>
                        <strong>Nome: </strong>{' '}
                        {`${part.name} ${part.lastName}`}
                      </p>
                      <p>
                        <strong>E-mail: </strong> {part.email}
                      </p>
                      <p>
                        <strong>CPF: </strong> {formatToCPF(part.cpf)}
                      </p>
                      <p>
                        <strong>Telefone: </strong> {formatToPhone(part.phone)}
                      </p>
                    </div>
                  ))}
              </div>
              <ActionButtons>
                <Button type="button">
                  <Link to={`/edit/${item.id}`}>
                    <FaEdit />
                    Editar
                  </Link>
                </Button>
                <Button
                  type="button"
                  delete
                  onClick={() => this.handleDelete(item.id)}
                >
                  <FaTrash />
                  Excluir
                </Button>
              </ActionButtons>
            </li>
          ))}
        </List>
      </Container>
    );
  }
}

export default Main;
