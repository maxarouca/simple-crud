import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Main from './pages/Main';
import AddEditContract from './pages/AddEditContract';
// import Repository from './pages/Repository';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <Route path="/" exact component={Main} />
        <Route path="/add/" component={AddEditContract} />
        <Route path="/edit/:id" component={AddEditContract} />
        {/* <Route path="/repository/:repository" component={Repository} /> */}
      </Switch>
    </Router>
  );
};

export default Routes;
