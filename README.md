# CRUD de Cadastro de Contratos

Aplicação desenvolvida no front-end com React e no back-end utilizando um pacote chamado `json-server` visando fornecer um CRUD simples de cadastro de contratos.

## Instalação e execução do projeto

Você precisará primeiramente ter o `NodeJS` instalado em sua máquina.

### Back-end

Primeiro, precisaremos iniciar o back-end do nosso projeto. Aqui, utilizamos a biblioteca `json-server`, que nos fornecerá uma API rest em poucos segundos. Para isso, vamos instalar essa biblioteca.

`npm install -g json-server` ou `yarn global add json-server`

Após instalada, já está configurado em seu package.json um comando para iniciar o back-end. Basta rodar:

`npm run server` ou `yarn server`

### Front-end

Para o front-end, utilizamos o create-react-app como ponto de partida. Para testar em sua máquina, primeiro, clone este repositório em uma pasta de sua preferência.

Após isso, pelo seu `bash` ou `terminal`, entre nessa pasta. Daí, precisaremos instalar os pacotes utilizados para o desenvolvimento do projeto. Para isso, basta rodar o seguinte comando no seu `terminal`:

`npm install` ou `yarn`

Após instaladas todas as dependências do projeto, para execução do mesmo, basta rodar em seu terminal o seguinte comando:

`npm run start` ou `yarn start`.


## Bibliotecas utilizadas no front-end

- axios
- brazilian-values
- date-fns
- formik
- moment
- prop-types
- react
- react-datepicker
- react-dom
- react-icons
- react-router-dom
- react-scripts
- react-toastify
- styled-components
